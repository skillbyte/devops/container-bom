FROM golang:1.21 as builder

WORKDIR /app

COPY . .

RUN go mod download \
    && go mod verify \
    && CGO_ENABLED=0 go build -o container-bom

FROM alpine:3.18.3

COPY --from=builder /app/container-bom /bin/container-bom

ENV CONTAINER_BOM_IN_CONTAINER 1

ENTRYPOINT [ "/bin/container-bom" ]
