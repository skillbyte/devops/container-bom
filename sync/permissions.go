package sync

import (
	"fmt"
	"io/fs"
	"os"
	"path"

	"gitlab.com/skillbyte/devops/container-bom/parse"
)

func calculatePermissions(currentPermissions int64, overwritePermissions *parse.OverwritePermissions) int64 {
	var desiredPermissions = currentPermissions
	if overwritePermissions == nil {
		return currentPermissions
	}
	if overwritePermissions.Read != nil {
		if *overwritePermissions.Read {
			desiredPermissions = desiredPermissions | 0444
		} else {
			desiredPermissions = desiredPermissions & 0333
		}
	}
	if overwritePermissions.Write != nil {
		if *overwritePermissions.Write {
			desiredPermissions = desiredPermissions | 0222
		} else {
			desiredPermissions = desiredPermissions & 0555
		}
	}
	if overwritePermissions.Execute != nil {
		if *overwritePermissions.Execute {
			desiredPermissions = desiredPermissions | 0111
		} else {
			desiredPermissions = desiredPermissions & 0666
		}
	}
	return desiredPermissions
}

func rectifyPermissions(fullPath string, overwritePermissions *parse.OverwritePermissions) error {
	var currentPermissions = readPermissions(fullPath)
	var desiredPermissions = calculatePermissions(currentPermissions, overwritePermissions)
	if currentPermissions != desiredPermissions {
		fmt.Fprintf(os.Stderr, "Setting permissions to %o for artifact: %s\n", desiredPermissions, path.Base(fullPath))
		return os.Chmod(fullPath, fs.FileMode(desiredPermissions))
	}
	return nil
}

func readPermissions(fullPath string) int64 {
	fileInfo, err := os.Stat(fullPath)
	if err != nil {
		return 0
	}
	return int64(fileInfo.Mode())
}
