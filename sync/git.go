package sync

import (
	"os/exec"

	lockfile "gitlab.com/skillbyte/devops/container-bom/lock-file"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

func gitAdd(artifact lockfile.Artifact) error {
	var cmd = exec.Command("git", "add", artifact.Path)
	var err = cmd.Run()
	return err
}

func GitCommit(bom parse.BillOfMaterials) error {
	var cmd = exec.Command("git", "commit", "-m", "[container-bom] sync artifacts")
	var err = cmd.Run()
	return err
}
