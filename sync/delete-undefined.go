package sync

import (
	"fmt"
	"os"
	"path/filepath"

	lockfile "gitlab.com/skillbyte/devops/container-bom/lock-file"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

func DeleteUndefinedArtifacts(bom parse.BillOfMaterials, gitAdd_ bool) (bool, error) {
	fileNames, err := os.ReadDir(bom.LocalPathSettings.CommonRootPath)
	var artifactDeleted = false
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error listing files in common-root-path: %s\n%s\naborting...", bom.LocalPathSettings.CommonRootPath, err.Error())
		return artifactDeleted, err
	}

	for _, fileName := range fileNames {
		var found = false
		for _, artifact := range bom.RequiredArtifacts {
			if fileName.Name() == artifact.StoragePath.Local {
				found = true
				break
			}
		}
		if !found {
			fmt.Fprintf(os.Stderr, "Deleting undefined artifact: %s\n", fileName.Name())
			var path = filepath.Join(bom.LocalPathSettings.CommonRootPath, fileName.Name())
			err = os.Remove(path)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error deleting artifact: %s\n%s\naborting...", fileName.Name(), err.Error())
				return artifactDeleted, err
			}
			artifactDeleted = true
			if gitAdd_ {
				err = gitAdd(lockfile.Artifact{Path: path})
				if err != nil {
					return artifactDeleted, err
				}
			}
		}
	}
	return artifactDeleted, nil
}
