package sync

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os"
	"path"
	"path/filepath"

	lockfile "gitlab.com/skillbyte/devops/container-bom/lock-file"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

type CheckError struct {
	path string
}

func (e *CheckError) Error() string {
	return fmt.Sprintf("Not in sync. Check failed on artifact: %s", e.path)
}

func SyncManualArtifact(
	baseDirectory string,
	oldLockfile lockfile.LockFile,
	newLockfile *lockfile.LockFile,
	requiredArtifact parse.RequiredArtifact,
	silent bool,
	check bool,
	add bool,
) (bool, error) {
	var fullPath = path.Join(baseDirectory, requiredArtifact.StoragePath.Local)
	var current_hash = calculateManualSha256(fullPath, requiredArtifact.OverwritePermissions)
	var newLockedArtifact = lockfile.Artifact{
		Path:   requiredArtifact.StoragePath.Local,
		Sha256: current_hash,
	}
	var artifactChanged = false
	for _, oldLockedArtifact := range oldLockfile.Artifacts {
		if oldLockedArtifact.Path == newLockedArtifact.Path {
			if oldLockedArtifact.Sha256 == newLockedArtifact.Sha256 {
				if !silent && !check {
					fmt.Fprintf(os.Stderr, "No change detected for artifact: %s\n", requiredArtifact.StoragePath.Local)
				}
				newLockfile.Artifacts = append(newLockfile.Artifacts, newLockedArtifact)
			} else if check {
				return artifactChanged, &CheckError{newLockedArtifact.Path}
			} else {
				fmt.Fprintf(os.Stderr, "Artifact changed, updating the stored hash: %s\n", requiredArtifact.StoragePath.Local)
				artifactChanged = true
				var err = rectifyPermissions(fullPath, requiredArtifact.OverwritePermissions)
				if err != nil {
					return artifactChanged, err
				}
				new_hash := calculateManualSha256(fullPath, requiredArtifact.OverwritePermissions)
				newLockedArtifact.Sha256 = new_hash
				newLockfile.Artifacts = append(newLockfile.Artifacts, newLockedArtifact)
				if add {
					err = gitAdd(newLockedArtifact)
					if err != nil {
						return artifactChanged, err
					}
				}
			}
			return artifactChanged, nil
		}
	}

	if check {
		return artifactChanged, &CheckError{newLockedArtifact.Path}
	}
	fmt.Fprintf(os.Stderr, "Adding artifact to lock file: %s\n", requiredArtifact.StoragePath.Local)
	newLockfile.Artifacts = append(newLockfile.Artifacts, newLockedArtifact)

	return artifactChanged, nil
}

func calculateManualSha256(fullPath string, overwritePermissions *parse.OverwritePermissions) string {
	artifactFile, err := os.ReadFile(fullPath)
	if err != nil {
		artifactFile = []byte{}
	}
	var currentPermissions = readPermissions(fullPath)
	var desiredPermissions = calculatePermissions(currentPermissions, overwritePermissions)
	// this is used as a special error value to trigger rectification of permissions and will never be persisted in the lock
	if currentPermissions != desiredPermissions {
		fmt.Fprintf(os.Stderr, "Permissions incorrect for artifact %s: expected %o, got %o", filepath.Base(fullPath), desiredPermissions, currentPermissions)
		currentPermissions = -1
	}

	var preHash = append(artifactFile, fmt.Sprint(currentPermissions)...)
	h := sha256.New()
	h.Write(preHash)
	return hex.EncodeToString(h.Sum(nil))
}
