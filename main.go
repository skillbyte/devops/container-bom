/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "gitlab.com/skillbyte/devops/container-bom/cmd"

func main() {
	cmd.Execute()
}
