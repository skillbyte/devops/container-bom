package parse

import "fmt"

const (
	Manual SourceType = "manual"
	Web    SourceType = "web"
)

type SourceError struct {
	sourceType SourceType
}

func (e *SourceError) Error() string {
	if e.sourceType == Manual {
		return "SourceType manual requires comment and forbids url"
	} else if e.sourceType == Web {
		return "SourceType web requires url and forbids comment"
	}
	return fmt.Sprintf("unsupported SourceType: %s", e.sourceType)
}

const (
	Copy AllowedTransferMethod = "copy"
	Add  AllowedTransferMethod = "add"
)

type AllowedTransferMethodError struct {
}

func (e *AllowedTransferMethodError) Error() string {
	return "Only copy and add are allowed values for transfer-method"
}
