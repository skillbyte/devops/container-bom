package parse

import (
	"encoding/json"
	"strings"
)

func (s *Source) UnmarshalJSON(text []byte) error {
	type source Source
	src := source{
		Type: "null",
	}
	if err := json.Unmarshal(text, &src); err != nil {
		return err
	}
	if (src.Type == Manual && (src.Comment == nil || src.URL != nil)) ||
		(src.Type == Web && (src.Comment != nil || src.URL == nil)) ||
		(src.Type != Manual && src.Type != Web) {
		return &SourceError{src.Type}
	}
	*s = Source(src)
	return nil
}

func (s *RequiredArtifact) UnmarshalJSON(text []byte) error {
	type requiredArtifact RequiredArtifact
	var reqArt requiredArtifact
	if err := json.Unmarshal(text, &reqArt); err != nil {
		return err
	}
	if reqArt.TransferMethod == "" {
		reqArt.TransferMethod = Copy
	}
	reqArt.TransferMethod = AllowedTransferMethod(strings.ToLower(string(reqArt.TransferMethod)))
	if reqArt.TransferMethod != Copy && reqArt.TransferMethod != Add {
		return &AllowedTransferMethodError{}
	}
	*s = RequiredArtifact(reqArt)
	return nil
}
