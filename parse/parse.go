package parse

import (
	"encoding/json"
	"os"

	"sigs.k8s.io/yaml"
)

type AllowedTransferMethod string
type SourceType string

type BillOfMaterials struct {
	LocalPathSettings LocalPathSettings  `json:"local-path-settings"`
	RequiredArtifacts []RequiredArtifact `json:"required-artifacts"`
}

type LocalPathSettings struct {
	CommonRootPath           string `json:"common-root-path"`
	DeleteUndefinedArtifacts bool   `json:"delete-undefined-artifacts"`
}

type RequiredArtifact struct {
	StoragePath          StoragePath           `json:"storage-path"`
	Source               Source                `json:"source"`
	TransferMethod       AllowedTransferMethod `json:"transfer-method,omitempty"`
	OverwritePermissions *OverwritePermissions `json:"overwrite-permissions,omitempty"`
}

type OverwritePermissions struct {
	Read    *bool `json:"read,omitempty"`
	Write   *bool `json:"write,omitempty"`
	Execute *bool `json:"execute,omitempty"`
}

type Source struct {
	Type    SourceType `json:"type"`
	Comment *string    `json:"comment,omitempty"`
	URL     *string    `json:"url,omitempty"`
}

type StoragePath struct {
	Local     string `json:"local"`
	Container string `json:"container"`
}

func ParseFromFile(filePath string) (BillOfMaterials, error) {
	yamlData, err := os.ReadFile(filePath)
	if err != nil {
		return BillOfMaterials{}, err
	}

	jsonData, err := yaml.YAMLToJSON(yamlData)
	if err != nil {
		return BillOfMaterials{}, err
	}

	var billOfMaterials BillOfMaterials
	err = json.Unmarshal(jsonData, &billOfMaterials)
	if err != nil {
		return BillOfMaterials{}, err
	}

	return billOfMaterials, nil
}
