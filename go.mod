module gitlab.com/skillbyte/devops/container-bom

go 1.21.0

require (
	github.com/c4milo/unpackit v1.0.0
	github.com/otiai10/copy v1.12.0
	sigs.k8s.io/yaml v1.3.0
)

require (
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/klauspost/compress v1.4.1 // indirect
	github.com/klauspost/cpuid v1.2.0 // indirect
	github.com/klauspost/pgzip v1.2.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)

require (
	github.com/cavaliergopher/grab/v3 v3.0.1
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/cobra v1.7.0
	github.com/spf13/pflag v1.0.5 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
