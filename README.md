# container-bom

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Manage vendored dependencies for container builds.

We often have static artifacts that we wish to include in images, be it certificates, binaries that aren't available via
packet manager etc. `container-bom` (BOM: Bill of Materials) uses a YAML config for such static files where both
manually added as well as automatically downloadable artifacts can be included. `container-bom` keeps track of the
artifacts by way of creating a `.container-bom.lock` lockfile and can automatically download files from the web when
the downloaded and versioned artifacts deviate from the tracked and configured files.

Make sure to include the artifacts `container-bom` downloads in git so that you're not depending on those file being
downloadable at image build time in the future. `container-bom` can `git add` and `git commit` artifacts for you.

Downloading and keeping track of artifacts is only half the battle; you also need to add the artifacts to container
images for them to be useful. `container-bom` provides two methods of doing this either with a separate build stage in
your Dockerfile or by templating the Dockerfile. The separate build step-method is also compatible with
`buildah`-based container build scripts, if you prefer that over a Dockerfile.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [YAML Configuration](#yaml-configuration)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Go 1.21 or higher is required for installing `container-bom`. Check the [Go documentation](https://go.dev/doc/install)
on how to install Go or use your favorite package manager.

```sh
go install gitlab.com/skillbyte/devops/container-bom@latest
```

### LFS

While it is optional we strongly recommend using `git-lfs` to version artifacts along with the Dockerfile. You can
follow
[these instructions](https://docs.github.com/en/repositories/working-with-files/managing-large-files/installing-git-large-file-storage)
to set up `git-lfs`. Remember to
[enable LFS if your repo is on GitLab](https://docs.gitlab.com/ee/topics/git/lfs/#enable-git-lfs-for-a-project). Repos
on GitHub support LFS out of the box.

To use `git-lfs` to version your artifacts run the following commands (replace the folder name if you're using a
different one):

```sh
git lfs track static-artifacts/
git add :/.gitattributes
```

You can then use the `container-bom sync -a` to run `git add` on artifacts or `container-bom sync -c` to `git add` and
`git commit` artifacts. See [below](#sync) for further information on the `sync` subcommand.

## Usage

This sections aims to give an overview over the subcommands of `container-bom` and their usage.

### `validate`

Run `container-bom validate` to check if a `container-bom.yaml` is valid.

### `sync`

Run `container-bom sync` to download artifacts with `source.type` `url`, fix the permissions if necessary and'
update the lockfile. If an artifact with `source.type` `manual` changes the change is noted in the output, permissions
are fixed if necessary and the lockfile gets updated as well.

Flags:

| short version | long version | description                                                            |
|---------------|--------------|------------------------------------------------------------------------|
| -b            | --bom string | Path to the container-bom configuration (default "container-bom.yaml") |
| -c            | --check-only | Only check for deviations but don't acutally sync anything             |
| -a            | --git-add    | automatically run git add changed artifacts                            |
| -C            | --git-commit | automatically create a git commit with changed artifacts, implies -a   |
| -h            | --help       | help for sync                                                          |

### `generate-commands`

`generate-commands` supports two format, `dockerfile` (default) and `buildah` to accommodate both Dockerfile and
`buildah` users.

Flags:

| short version | long version        | description                                                                                   |
|---------------|---------------------|-----------------------------------------------------------------------------------------------|
| -b            | --bom string        | Path to the container-bom configuration (default "container-bom.yaml")                        |
| -c            | --container string  | Name or ID for the buildah container, only required if --format=buildah, otherwise unused     |
| -f            | --format dockerfile | Format of the COPY/ADD commands, can be either dockerfile or `buildah` (default "dockerfile") |
| -h            | --help              | help for generate-commands                                                                    |

#### Format `dockerfile`

Run `container-bom generate-commands` to generate the Dockerfile instructions to `COPY` and `ADD`
all artifacts. This subcommand is intended for use in templated Dockerfiles like this minimal eRuby example:

```erb
FROM alpine

<%=
result = `container-bom generate-commands`
if $?.exitstatus != 0 then
  exit 1
end
result
%>

RUN echo whatever
```

Assuming the above script is saved as `Dockerfile.erb` it can be executed by running
`erb Dockerfile.erb | docker build -t my-image-name -f - .`

#### Format `buildah`

To generate and execute `buildah copy` and `buildah add` commands instead of the respective `dockerfile`-formatted
commands you can use `-f buildah` like so:

```sh
container-bom generate-commands -f buildah -c $container | sh
```

The name or ID of the `buildah` container used for the commands is required when using `-f buildah` and can
be specified with the `-c`/`--container` flag.

### `transfer`

Run `container-bom transfer -t target` to transfer the artifacts to the specified
directory. Artifacts with `transfer-method` `copy` will simply be copied whereas artifacts with `transfer-method` `add`
will be unpacked into the target directory. Every `storage-path.container` (even explicitly absolute paths) will be
relative to the target path.

This subcommand is intended to run in its own build stage in a Dockerfile and will not run outside of the
`kfkskillbyte/container-bom` container unless you overwrite this behavior by setting the environment variable
`CONTAINER_BOM_IN_CONTAINER` to `1`.

Flags:

| short version | long version              | description                                                                        |
|---------------|---------------------------|------------------------------------------------------------------------------------|
| -b            | --bom string              | Path to the container-bom configuration (default "container-bom.yaml")             |
| -h            | --help                    | help for transfer                                                                  |
| -t            | --target-path-root string | A target path that is used as the root for all transfers (even for absolute paths) |

#### `transfer` example

The following is a minimal example for a two-stage Dockerfile with a `container-bom` stage:

```Dockerfile
FROM kfkskillbyte/container-bom AS static-artifacts

COPY container-bom.yaml /container-bom.yaml
COPY .container-bom.lock /.container-bom.lock

# this assumes the directory for the artifacts to be called static-artifacts, change if necessary
RUN --mount=source=static-artifacts,target=/static-artifacts \
    container-bom transfer -t target

FROM alpine

COPY --from=static-artifacts /target /

RUN echo whatever
```

This Dockerfile is functionally equivalent to [the Dockerfile template above](#format-dockerfile) with one minor
difference: Copying the artifacts from a previous build stage only creates one layer whereas in the templated Dockerfile
each `COPY` or `ADD` instruction creates a new layer.

### `help`

Print the help text for `container-bom` or its subcommands.

Flags:

| Key | Required | Explanation   |
|-----|----------|---------------|
| -h  | --help   | help for help |

### `completion`

Generate completions for  `bash`, `fish`, `powershell` and `zsh`.

Flags:

| Key | Required | Explanation         |
|-----|----------|---------------------|
| -h  | --help   | help for completion |

## YAML configuration

Using container-bom requires the use of a `container-bom.yaml` as its bill of materials. This section specifies the
format of the bill of materials.

The bill of materials features two top level keys: `local-path-settings` and `required-artifacts`.

### `local-path-settings`

`local-path-settings` is a map with the following keys:

| Key                          | Required | Explanation                                                                                                                                |
|------------------------------|----------|--------------------------------------------------------------------------------------------------------------------------------------------|
| `common-root-path`           | yes      | Common root path for all local artifact paths. All artifacts will be stores here.                                                          |
| `delete-undefined-artifacts` | no       | Boolean, allows automatic deletion of artifacts found in the common root path but not defined in `container-bom.yaml`. Defaults to `false` |

### `required-artifacts`

`required-artifacts` is a list of the required artifacts. Each artifact is a map with the following keys and sub-keys:

| Key                             | Required  | Explanation                                                                                                                                                            |
|---------------------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `storage-path.local`            | yes       | local path of the artifact, relative to common-root-path                                                                                                               |
| `storage-path.container`        | yes       | absolute path of the artifact in the container                                                                                                                         |
| `source.type`                   | yes       | Must be either `manual` for manually managed artifacts or `web` for downloadable artifacts                                                                             |
| `source.comment`                | sometimes | Required if `source.type` is `manual`. Use this field to give hints as to how to (re)generate or where to find the artifact should you need to                         |
| `source.url`                    | sometimes | Required if `source.type` is `web`. The download URL for the artifact                                                                                                  |
| `transfer-method`               | no        | Can be either `add` or `copy`, defaults to `copy`. `add` will decompress archives, much like the Dockerfile instruction `ADD`, while `copy` merely copies the artifact |
| `overwrite-permissions.read`    | no        | Boolean, set read permissions on artifacts if `true`, revoke if `false`, do nothing if ommitted                                                                        |
| `overwrite-permissions.write`   | no        | Boolean, set write permissions on artifacts if `true`, revoke if `false`, do nothing if ommitted                                                                       |
| `overwrite-permissions.execute` | no        | Boolean, set execute permissions on artifacts if `true`, revoke if `false`, do nothing if ommitted                                                                     |

### Example configuration

This is just an exemplary bill of materials, please refer to the above subsections for a complete specification.

```yaml
local-path-settings:
  common-root-path: static-artifacts
  delete-undefined-artifacts: true
required-artifacts:
- storage-path:
    local: my-secret-certificate.crt
    container: /usr/local/share/ca-certificates/
  source:
    type: manual
    comment: This is how to (re)generate the certificate
- storage-path:
    local: helm
    container: /usr/local/bin/helm-dir
  source:
    type: web
    url: https://get.helm.sh/helm-v3.8.2-linux-arm64.tar.gz
  transfer-method: add
  overwrite-permissions:
    execute: yes
```

## Maintainers

[@kfkonrad](https://gitlab.com/kfkonrad)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Kevin F. Konrad
