package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	lockfile "gitlab.com/skillbyte/devops/container-bom/lock-file"
	"gitlab.com/skillbyte/devops/container-bom/parse"
	"gitlab.com/skillbyte/devops/container-bom/sync"
)

var bomPathSync string
var silentSync bool
var checkOnlySync bool
var commit bool
var add bool

// syncCmd represents the sync command
var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Download artifacts and update lock file",
	Long:  `Download artifacts and update lock file`,
	Run: func(cmd *cobra.Command, args []string) {
		bom, err := parse.ParseFromFile(bomPathSync)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error parsing %s: %s", bomPathSync, err.Error())
			os.Exit(1)
		}
		lockFile, err := lockfile.Read()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading the log file from %s/.bom.lock: %s", bomPathSync, err.Error())
			os.Exit(1)
		}

		newLockfile := lockfile.LockFile{}
		var anyArtifactChanged = false

		for _, artifact := range bom.RequiredArtifacts {
			var err error
			var currentArtifactChanged bool
			if artifact.Source.Type == parse.Manual {
				currentArtifactChanged, err = sync.SyncManualArtifact(bom.LocalPathSettings.CommonRootPath, lockFile, &newLockfile, artifact, silentSync, checkOnlySync, add || commit)
			}
			if artifact.Source.Type == parse.Web {
				currentArtifactChanged, err = sync.SyncWebArtifact(bom.LocalPathSettings.CommonRootPath, lockFile, &newLockfile, artifact, silentSync, checkOnlySync, add || commit)
			}
			anyArtifactChanged = anyArtifactChanged || currentArtifactChanged
			if err != nil {
				if checkOnlySync {
					fmt.Fprintf(os.Stderr, "Error checking artifact: %s\n%s\naborting...\n", artifact.StoragePath.Local, err.Error())
				} else {
					fmt.Fprintf(os.Stderr, "Error syncing artifact: %s\n%s\naborting...\n", artifact.StoragePath.Local, err.Error())
				}
				os.Exit(1)
			}
		}

		if bom.LocalPathSettings.DeleteUndefinedArtifacts {
			deletedArtifacts, err := sync.DeleteUndefinedArtifacts(bom, add || commit)
			anyArtifactChanged = anyArtifactChanged || deletedArtifacts
			if err != nil {
				os.Exit(1)
			}
		}

		if !checkOnlySync {
			lockfile.Write(newLockfile)
		}

		if !checkOnlySync && commit {
			if anyArtifactChanged {
				sync.GitCommit(bom)
			} else {
				fmt.Fprintln(os.Stderr, "No artifacts have changed, skipping commit")
			}
		}
	},
}

func init() {
	syncCmd.Flags().StringVarP(&bomPathSync, "bom", "b", bomPathDefaultValue, bomPathHelpText)
	syncCmd.Flags().BoolVarP(&silentSync, "silent", "s", false, "Suppress text output where no change is detected")
	syncCmd.Flags().BoolVarP(&checkOnlySync, "check-only", "c", false, "Only check for deviations but don't acutally sync anything")
	syncCmd.Flags().BoolVarP(&commit, "git-commit", "C", false, "automatically create a git commit with changed artifacts, implies -a")
	syncCmd.Flags().BoolVarP(&add, "git-add", "a", false, "automatically run git add changed artifacts")
	rootCmd.AddCommand(syncCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// syncCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// syncCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
