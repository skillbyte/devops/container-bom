package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/c4milo/unpackit"
	"github.com/otiai10/copy"
	"github.com/spf13/cobra"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

var targetPath string
var bomPathTransfer string

var transferCmd = &cobra.Command{
	Use:   "transfer",
	Short: "transfer all artifacts to a target directory in a container",
	Long:  `transfer all artifacts to a target directory in a container`,
	Run: func(cmd *cobra.Command, args []string) {
		checkOnlySync = true
		syncCmd.Run(cmd, args)

		bom, err := parse.ParseFromFile(bomPathTransfer)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error parsing %s: %s", bomPathTransfer, err.Error())
			os.Exit(1)
		}

		for _, artifact := range bom.RequiredArtifacts {
			var source = filepath.Join(bom.LocalPathSettings.CommonRootPath, artifact.StoragePath.Local)
			var destination = filepath.Join(targetPath, artifact.StoragePath.Container)
			var CONTAINER_BOM_IN_CONTAINER = os.Getenv("CONTAINER_BOM_IN_CONTAINER")
			if CONTAINER_BOM_IN_CONTAINER != "1" {
				fmt.Println("This command can only run in the container-bom container, set environment variable CONTAINER_BOM_IN_CONTAINER to 1 to overwrite.")
			}
			if artifact.TransferMethod == parse.Copy {
				err := copy.Copy(source, destination)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error reading artifact %s: %s, aborting...\n", artifact.StoragePath.Local, err.Error())
					os.Exit(1)
				}
			} else if artifact.TransferMethod == parse.Add {
				tarball, err := os.Open(source)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error reading artifact %s: %s, aborting...\n", artifact.StoragePath.Local, err.Error())
					os.Exit(1)
				}
				err = unpackit.Unpack(tarball, destination)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error unpacking artifact %s: %s, aborting...\n", artifact.StoragePath.Local, err.Error())
					os.Exit(1)
				}
			} else {
				fmt.Fprintf(os.Stderr, "Error: no such transfer-method %s for artifact %s\n", artifact.TransferMethod, artifact.StoragePath.Local)
				os.Exit(1)
			}
		}
	},
}

func init() {
	transferCmd.Flags().StringVarP(&bomPathTransfer, "bom", "b", bomPathDefaultValue, bomPathHelpText)
	transferCmd.Flags().StringVarP(&targetPath, "target-path-root", "t", "", "A target path that is used as the root for all transfers (even for absolute paths)")
	transferCmd.MarkFlagRequired("target-path-root")
	rootCmd.AddCommand(transferCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// dockerTransferCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// dockerTransferCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
