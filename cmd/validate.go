package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

var silentValidate bool
var bomPathValidate string

// validateCmd represents the validate command
var validateCmd = &cobra.Command{
	Use:   "validate [bom to validate]",
	Short: "Validate a bom.yaml configuration",
	Long:  `Parse the given bom.yaml configuration and report any error encountered.`,
	Run: func(cmd *cobra.Command, args []string) {
		var fileName = args[0]
		_, err := parse.ParseFromFile(fileName)
		if err != nil {
			if !silentValidate {
				fmt.Fprintf(os.Stderr, "Inavlid file %s: %s\n", fileName, err.Error())
			}
			os.Exit(1)
		}
		if !silentValidate {
			fmt.Fprintf(os.Stderr, "Validation of %s successful.\n", fileName)
		}
	},
}

func init() {
	validateCmd.Flags().StringVarP(&bomPathValidate, "bom", "b", bomPathDefaultValue, bomPathHelpText)
	validateCmd.Flags().BoolVarP(&silentValidate, "silent", "s", false, "Suppress all text output.")
	rootCmd.AddCommand(validateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// validateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// validateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
