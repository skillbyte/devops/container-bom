package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/skillbyte/devops/container-bom/parse"
)

var bomPathGenerate string
var formatGenerate string
var containerGenerate string

var generateCommandsCmd = &cobra.Command{
	Use:   "generate-commands",
	Short: "generate COPY and ADD commands for use in Dockerfile or with buildah",
	Long:  `generate COPY and ADD commands for use in Dockerfile or with buildah`,
	Run: func(cmd *cobra.Command, args []string) {
		checkOnlySync = true
		bomPathSync = bomPathGenerate
		syncCmd.Run(cmd, args)

		bom, err := parse.ParseFromFile(bomPathGenerate)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error parsing %s: %s", bomPathGenerate, err.Error())
			os.Exit(1)
		}

		var formatString string
		var methodTransformation func(string) string
		if formatGenerate == "dockerfile" {
			formatString = "%s %s %s\n"
			methodTransformation = strings.ToUpper
		} else if formatGenerate == "buildah" {
			if containerGenerate == "" {
				fmt.Fprintln(os.Stderr, "--container not supplied but require with --format=buildah")
				os.Exit(1)
			}
			formatString = fmt.Sprintf("buildah %%s %s %%s %%s\n", containerGenerate)
			methodTransformation = strings.ToLower
		} else {
			fmt.Fprintf(os.Stderr, "--format=%#v not allowed, see help for possible values\n", formatGenerate)
			os.Exit(1)
		}
		for _, artifact := range bom.RequiredArtifacts {
			var source = filepath.Join(bom.LocalPathSettings.CommonRootPath, artifact.StoragePath.Local)
			var upperTransferMethod = methodTransformation(string(artifact.TransferMethod))
			fmt.Printf(formatString, upperTransferMethod, source, artifact.StoragePath.Container)
		}
	},
}

func init() {
	generateCommandsCmd.Flags().StringVarP(&bomPathGenerate, "bom", "b", bomPathDefaultValue, bomPathHelpText)
	generateCommandsCmd.Flags().StringVarP(&formatGenerate, "format", "f", "dockerfile", "Format of the COPY/ADD commands, can be either `dockerfile` or `buildah`")
	generateCommandsCmd.Flags().StringVarP(&containerGenerate, "container", "c", "", "Name or ID for the buildah container, only required if --format=buildah, otherwise unused")
	rootCmd.AddCommand(generateCommandsCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateDockerfileCommandsCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateDockerfileCommandsCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
