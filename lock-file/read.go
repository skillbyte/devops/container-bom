package lockfile

import (
	"encoding/json"
	"errors"
	"os"
	"path"
)

type LockFile struct {
	Artifacts []Artifact `json:"artifacts"`
}

type Artifact struct {
	Path   string `json:"path"`
	Sha256 string `json:"sha256"`
}

func Read() (LockFile, error) {
	var bomPath = path.Join(".", ".container-bom.lock")
	if _, err := os.Stat(bomPath); err == nil {
		rawLockFile, err := os.ReadFile(bomPath)
		if err != nil {
			return LockFile{}, err
		}
		var lockFile LockFile
		err = json.Unmarshal(rawLockFile, &lockFile)
		if err != nil {
			return LockFile{}, err
		}
		return lockFile, nil
	} else if errors.Is(err, os.ErrNotExist) {
		return LockFile{}, nil
	} else {
		return LockFile{}, err
	}
}
