package lockfile

import (
	"encoding/json"
	"os"
	"path"
)

func Write(lockFile LockFile) error {
	var bomPath = path.Join(".", ".container-bom.lock")
	jsonLockFile, err := json.Marshal(lockFile)
	if err != nil {
		return err
	}
	err = os.WriteFile(bomPath, jsonLockFile, 0644)
	return err
}
